from flask import Flask, Response
from gevent.pywsgi import WSGIServer
import json
import jsonify

app = Flask(__name__)
print(__name__)

books = [
    {}
]

@app.route('/hello')
def hello_world():
    data = {
        'hello': 'world',
        'number': 3
    }
    js = json.dumps(data)
    resp = Response(js, status=200, mimetype='application/json')
    return resp


@app.route('/book/<int:isbn>')
def get_book(isbn):
    data = {
        'hello': 'world',
        'number': 3,
        'isbn': isbn
    }
    js = json.dumps(data)
    resp = Response(js, status=200, mimetype='application/json')
    return resp


if __name__ == '__main__':
    print('Serving on 8088...')
    WSGIServer(('127.0.0.1', 8088), application=app).serve_forever()
    